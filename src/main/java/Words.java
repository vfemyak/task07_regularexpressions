public class Words {
    private String word;

    public Words(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public String toString() {
        return word;
    }

    public String removeCharAt(String s, int pos) {
        return s.substring(0, pos) + s.substring(pos + 1);
    }

    public boolean isPalindrome() {
            int n = this.word.length();
            for (int i = 0; i < (n/2); ++i) {
                if (this.word.charAt(i) != this.word.charAt(n - i - 1)) {
                    return false;
                }
            }

            return true;
    }
}
