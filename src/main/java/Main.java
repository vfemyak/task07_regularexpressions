import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

/**Початкові операції над Текстом (зчитування, розбивка на Реченння, Речення на Слова)**/
        Text text1 = new Text();
        String filePath1 = "D:\\Intellij IDEA\\task07_RegularExpressions\\src\\main\\resources\\textFile2.txt";
        try {
            text1.setText(Text.readLineByLine(filePath1));
            System.out.println("\nFile contains: \n");
        } catch (IOException e) {
            System.out.println("Wrong file path!");
            return;
        }

        List<Sentences> sentencesList = text1.extractSentenceFromText();    //всі речення тексту
        List<List<Words>> wordsList = new ArrayList<>();                    //всі слова тексту
        for (Sentences s : sentencesList) { //розбивка на речення
            //System.out.println(s);
            wordsList.add(s.extractWordsFromSentence());
            System.out.println("\tWords in sentence " + wordsList.size()
                    + ": " + wordsList.get(wordsList.size()-1));
        }
///////////////task01
//        List<Integer> difWordCount = new ArrayList<>();
//
//        for (List<Words> wrd :wordsList){
//            difWordCount.add(task1(wrd));
//        }
//
//        int max = difWordCount.get(0);
//        int pos = 0;
//        for (int i = 0; i < difWordCount.size(); i++)
//            if (difWordCount.get(i)>max) {
//                max = difWordCount.get(i);
//                pos = i;
//            }
//        System.out.println("\nTASK_01");
//        System.out.println("Sentence witch has the biggest amount of the same words: \n" + sentencesList.get(pos));
/////////////task02
//        Collections.sort(sentencesList, new Comparator<Sentences>() {
//            @Override
//            public int compare(Sentences o1, Sentences o2) {
//                return o1.getSentence().compareTo(o2.getSentence());
//            }
//        });
/////////////task06
//        System.out.println("\nTASK_06");
//        task6(wordsList);
/////////////task15
        //System.out.println("\nTASK_15");
        //task15(wordsList);
/////////////task14
        System.out.println("\nTASK_014");
        task14(wordsList);

        
        //System.out.println(Sentences.extractSentenceFromText(text1));

        //System.out.println(text1.toString());

    }

    public static int task1(List<Words> wordsList){
        Set<String> uniqWords = new HashSet<>();

        for (Words wrd : wordsList){
            uniqWords.add(wrd.getWord().toLowerCase());
        }

        return wordsList.size() - uniqWords.size();
    }

    public static void task6 (List<List<Words>> lists){
        List<Words> allWords = new ArrayList<>();

        for (List<Words>words:lists){
            allWords.addAll(words);     //об'єднуємо слова в один ліст
        }

        Collections.sort(allWords, new Comparator<Words>() {
                    @Override
                    public int compare(Words o1, Words o2) {
                        return o1.getWord().toLowerCase().compareTo(o2.getWord().toLowerCase());
                    }
                });

        char letter = allWords.get(0).getWord().toLowerCase().charAt(0);
        for (Words words: allWords){
            if (words.getWord().toLowerCase().charAt(0) == letter)
                System.out.print(words.getWord() + ",");
            else {
                System.out.println();
                letter = words.getWord().toLowerCase().charAt(0);
            }
        }
    }

    public static void task15(List<List<Words>> lists){ //потрібно пропустити перший символ
        List<Words> allWords = new ArrayList<>();
        List<String> changedWords = new ArrayList<>();

        for (List<Words>words:lists){
            allWords.addAll(words);     //об'єднуємо слова в один ліст
        }

        char letter;
        for (Words words: allWords){
            letter = words.getWord().toLowerCase().charAt(0);
            changedWords.add(words.getWord().replace(letter,'_'));    //заміняємо letter на _
        }

        System.out.println(changedWords.toString());
    }

    public static void task14(List<List<Words>> lists){
        List<Words> allWords = new ArrayList<>();

        for (List<Words>words:lists){
            allWords.addAll(words);     //об'єднуємо слова в один ліст
        }

        List<Words> Palindrome = new ArrayList<>();
        for (Words words : allWords){
            if (words.isPalindrome() && words.getWord().length() > 2){  //додаємо всі паліндроми
                Palindrome.add(words);
            }
        }

        if (Palindrome.size() != 0) {   //якщо є хоч один паліндром
            Words maxPalindrome = Palindrome.get(0);
            for (Words pal : Palindrome) {
                if (pal.getWord().length() > maxPalindrome.getWord().length())
                    maxPalindrome = pal;
            }
            System.out.println("The longest palindrome is " + maxPalindrome.getWord());
        }
        else
            System.out.println("There is no palindrome in the text");
    }

}
