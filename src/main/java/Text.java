import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Text {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static String readLineByLine(String filePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            reader.close();
            return stringBuilder.toString().replaceAll("[\\s]+", " ");
        }
    }

    public List<Sentences> extractSentenceFromText(){
        List<Sentences> sentences = new ArrayList<>();
        String[] sentencesList;

        sentencesList = this.getText().split("[\\.?!…]+\\s+");     //to avoid .Net and whitespace

        int i = 0;  //sentence id
        for (String s: sentencesList) {
            i++;
            sentences.add(new Sentences(s,i));
        }
        return sentences;
    }

    @Override
    public String toString() {
        return "Text:" +
                "\n'" + text + '\'';
    }


}

