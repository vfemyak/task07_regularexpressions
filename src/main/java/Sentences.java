import java.util.ArrayList;
import java.util.List;

public class Sentences {

    private String sentence;

    public int getSentence_id() {
        return sentence_id;
    }

    public void setSentence_id(int sentence_id) {
        this.sentence_id = sentence_id;
    }

    private int sentence_id;

    public Sentences(String sentence, int sentence_id) {
        this.sentence = sentence;
        this.sentence_id = sentence_id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Words> extractWordsFromSentence(){
        List<Words> words = new ArrayList<>();
        String[] wordsList;

        wordsList = this.getSentence().split("[\\.,\\s;:()/…«»—\"]+");

        for (String s: wordsList)
            words.add(new Words(s));

        return words;
    }

    @Override
    public String toString() {
        return "Sentence: {" +
                sentence + "}";
    }
}

